See https://github.com/frenetic-lang/pyretic/issues/26 for steps for
reproducing.

The most convenient way to install Pyretic is to use their VM, available at:
http://frenetic-lang.org/pyretic/
